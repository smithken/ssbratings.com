FROM eboraas/apache:latest

RUN apt-get update && apt-get install -y git

RUN git clone https://gitlab.com/smithken/ssbratings.com.git

RUN cp -r ssbratings.com/app/* /var/www/html/

EXPOSE 80