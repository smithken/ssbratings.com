(function(){
    var App = angular.module('App', ['chart.js']);

    App.controller('clickOnName', function ($scope){
        $scope.setRecentSearch = function(value){
            $scope.$parent.recentSearch = value;
        };
    });

    App.controller('ListController', function($scope, $http) {
        $http.get('data.json')
            .then(function(res){
                $scope.data = res.data.sort(function (a, b){return b.elo - a.elo;});
            })
            .then(function(){
                $scope.players = $scope.data.map(function(obj){
                    return obj.players;
                });
            })
            .then(function(){
                $scope.history = $scope.data.map(function(obj){
                    return obj.history;
                })
            })
            .then(function(){
                $scope.recent = $scope.data.map(function(obj){
                    return obj.recent;
                })
            });
    });

    App.controller('ChartController', function($scope, $http, $filter){
        $http.get('data.json')
            .then(function(res){
                $scope.data = res.data.sort(function (a, b){return b.elo - a.elo;});
            })
            .then(function(){
                $scope.playerstest = $scope.data.map(function(obj){
                    return obj.player;
                });
            })
            .then(function(){
                $scope.histories = $scope.data.map(function(obj){
                    return obj.history.map(function(obj){
                        return $filter('NumberNoCommaFilter')(obj.elo);
                    });
                });
            })
            .then(function(){
                $scope.historiesLimited = $filter('limitTo')($scope.histories, 4);
            });
        $scope.labels = ["SSS 35", "SSS 36", "SSS 37", "SSS 38", "SSS 39", "SSS 40", "SSS 41", "SSS 42", "SSS 43", "SSS 44", "SSS 45", "SSS 46", "SSS 47", "SSS 48"];

        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };
    });

    App.controller('TabController', function($scope){
        $scope.tab = 1;

        $scope.setTab = function(setTab){
            $scope.tab = setTab;
        };

        $scope.isSet = function(tabName){
            return $scope.tab === tabName;
        };
    });

    App.filter('NumberNoCommaFilter', function() {
        return function(value) {
            return parseInt(value, 10)
        }
    });

    App.filter('PlayerSearch', function() {
        return function(value) {
            return $scope.searchBox === value;
        }
    });

})();